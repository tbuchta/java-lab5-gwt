/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.buchta.tomasz.shared;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author tbuchta
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({pl.polsl.buchta.tomasz.shared.Vector3dTest.class})
public class Vector3dTestSuite {
}
