/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.buchta.tomasz.shared;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * Tests for Vector3d class.
 * @author Tomasz Buchta
 * @version 1.0
 * 
 */
public class Vector3dTest {
    
    public Vector3dTest() {
    }

    /**
     * Test of dot method, of class Vector3d.
     */
    @Test
    public void testDot() {
        System.out.println("dot");
        Vector3d b = new Vector3d(1,2,3);
        Vector3d instance = new Vector3d(4,5,6);
        Double expResult = 32d;
        Double result = instance.dot(b);
        assertEquals(expResult, result,0.01);
    }

    /**
     * Test of cross method, of class Vector3d.
     */
    @Test
    public void testCross() {
        System.out.println("cross");
        Vector3d a = new Vector3d(3,4,5);
        Vector3d b = new Vector3d(4,3,5);
        Vector3d result = a.cross(b);
        assertEquals( 5, result.getX(), 0.01);
        assertEquals( 5, result.getY(), 0.01);
        assertEquals(-7, result.getZ(), 0.01);
    }
    
}
