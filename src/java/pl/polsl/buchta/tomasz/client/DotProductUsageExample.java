/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.buchta.tomasz.client;

import com.google.gwt.core.client.GWT;

import com.google.gwt.user.client.rpc.AsyncCallback;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import pl.polsl.buchta.tomasz.shared.Vector3d;

/**
 * class using the DotProduct service.
 *
 * @author Tomasz Buchta
 * @version 1.0
 */
public class DotProductUsageExample extends VerticalPanel {

    private Label lblServerReply = new Label();
    private TextBox a1 = new TextBox();
    private TextBox a2 = new TextBox();
    private TextBox a3 = new TextBox();
    private TextBox b1 = new TextBox();
    private TextBox b2 = new TextBox();
    private TextBox b3 = new TextBox();
    private Button btnSend = new Button("Calculate dot product");
    
    /**
     * Parameterless constructor
     */
    public DotProductUsageExample() {
        add(new Label("a1: "));
        add(a1);
        
        add(new Label("a2: "));
        add(a2);
        
        add(new Label("a3: "));
        add(a3);
        
        add(new Label("b1: "));
        add(b1);
        
        add(new Label("b2: "));
        add(b2);
        
        add(new Label("b3: "));
        add(b3);
        
        add(btnSend);
        add(lblServerReply);

        // Create an asynchronous callback to handle the result.
        final AsyncCallback<Double> callback = new AsyncCallback<Double>() {
            public void onSuccess(Double result) {
                lblServerReply.setText("Result: " + result.toString());
            }
            
            public void onFailure(Throwable caught) {
                lblServerReply.setText("Communication failed" + caught.getLocalizedMessage());
            }
        };

        // Listen for the button clicks
        btnSend.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                // Make remote call. Control flow will continue immediately and later
                // 'callback' will be invoked when the RPC completes.
                try {
                    Double da1,da2,da3,db1,db2,db3;
                    da1 = Double.parseDouble(a1.getText());
                    da2 = Double.parseDouble(a2.getText());
                    da3 = Double.parseDouble(a3.getText());
                    db1 = Double.parseDouble(b1.getText());
                    db2 = Double.parseDouble(b2.getText());
                    db3 = Double.parseDouble(b3.getText());
                    Vector3d a = new Vector3d(da1,da2,da3);
                    Vector3d b = new Vector3d(db1,db2,db3);
                    
                    getService().calculateDotProduct(a,b,callback);
                }
                catch(NumberFormatException e){
                    Window.alert("Wrong input format " + e.getLocalizedMessage());
                }

            }
        });
    }
    
    /**
     * Gets the service from GWT
     * @return service instance
     */
    public static DotProductAsync getService() {
        // Create the client proxy. Note that although you are creating the
        // service interface proper, you cast the result to the asynchronous
        // version of the interface. The cast is always safe because the
        // generated proxy implements the asynchronous interface automatically.

        return GWT.create(DotProduct.class);
    }
}
