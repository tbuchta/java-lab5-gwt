/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.buchta.tomasz.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import pl.polsl.buchta.tomasz.shared.Vector3d;

/**
 * Cross product service async interface
 * @author Tomasz Buchta
 * @version 1.0
 */
public interface CrossProductAsync {

    public void calculateCrossProduct(Vector3d a,Vector3d b, AsyncCallback<Vector3d> callback);
}
