/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.buchta.tomasz.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import pl.polsl.buchta.tomasz.shared.Vector3d;

/**
 * Cross product remote service interface
 * @author Tomasz Buchta
 * @version 1.0
 */
@RemoteServiceRelativePath("crossproduct")
public interface CrossProduct extends RemoteService {

    public Vector3d calculateCrossProduct(Vector3d a,Vector3d b);
}
