package pl.polsl.buchta.tomasz.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point class for main module
 * @author Tomasz Buchta
 * @version 1.0
 */

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Main implements EntryPoint {
  /**
   * The message displayed to the user when the server cannot be reached or
   * returns an error.
   */
  private static final String SERVER_ERROR = "An error occurred while "
      + "attempting to contact the server. Please check your network "
      + "connection and try again.";

  /**
   * This is the entry point method.
   */
  public void onModuleLoad() {
    RootPanel.get().add(new DotProductUsageExample());
    RootPanel.get().add(new CrossProductUsageExample());
  }
}
