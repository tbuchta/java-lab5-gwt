/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.buchta.tomasz.shared;

import java.io.Serializable;

/**
 *
 * Simple Vector3d class providing vector dot and cross product operations.
 * @author Tomasz Buchta
 * @version 1.0
 */
public class Vector3d implements Serializable{
    /**
     * X component of vector
     */
    private double x;
    
    /**
     * Y component of vector
     */
    private double y;
    
    /**
     * Z component of vector
     */
    private double z;
    
    public Vector3d(){
        x = 0;
        y = 0;
        z = 0;
    }
    /**
     * Constructor accepting X,Y,Z coordinates of vector
     * @param x coordinate of vector
     * @param y coordinate of vector
     * @param z coordinate of vector
     */
    public Vector3d(double x,double y,double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    /**
     * Calculates Dot product of vector
     * @param b second vector
     * @return value of dot product
     */
    public Double dot(Vector3d b){
        return x * b.getX() + y * b.getY() + z * b.getZ();
    }
    /**
     * Calculates Cross product of vector
     * @param b second vector
     * @return new vector with value of cross product
     */
    public Vector3d cross(Vector3d b){
        return new Vector3d(
            y * b.getZ() - z * b.getY(),
            z * b.getX() - x * b.getZ(),
            x * b.getY() - y * b.getX()
        );
    }

    /**
     * Get X component of vector
     * @return the X component of vector
     */
    public double getX() {
        return x;
    }

    /**
     * Sets the X component of vector
     * @param x the X component of vector
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * Get Y component of vector
     * @return the Y component of vector
     */
    public double getY() {
        return y;
    }

    /**
     * Sets the Y component of vector
     * @param y the Y component of vector
     */
    public void setY(double y) {
        this.y = y;
    }

    /**
     * Get Z component of vector
     * @return the Z component of vector
     */
    public double getZ() {
        return z;
    }

    /**
     * Sets the Z component of vector
     * @param z the Z component of vector
     */
    public void setZ(double z) {
        this.z = z;
    }
    
    @Override
    public String toString(){
        return "<" + x + ", " +  y + " ," + z + ">";
    }
    
}
