/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.buchta.tomasz.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import pl.polsl.buchta.tomasz.client.DotProduct;
import pl.polsl.buchta.tomasz.shared.Vector3d;

/**
 *
 * Implementation of DotProduct remote service
 * @author Tomasz Buchta
 * @version 1.0
 * 
 */
public class DotProductImpl extends RemoteServiceServlet implements DotProduct {

    public Double calculateDotProduct(Vector3d a,Vector3d b) {
        return a.dot(b);
    }
}
