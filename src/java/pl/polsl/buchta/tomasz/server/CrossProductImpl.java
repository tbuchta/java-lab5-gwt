/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.buchta.tomasz.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import pl.polsl.buchta.tomasz.client.CrossProduct;
import pl.polsl.buchta.tomasz.shared.Vector3d;

/**
 *
 * Implementation of CrossProduct remote service
 * @author Tomasz Buchta
 * @version 1.0
 * 
 */
public class CrossProductImpl extends RemoteServiceServlet implements CrossProduct {

    public Vector3d calculateCrossProduct(Vector3d a, Vector3d b) {
        return a.cross(b);
    }
}
